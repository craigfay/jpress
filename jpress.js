const fs = require('fs');

const minify = (src, dest) => {
    
    // Define reduction rules
    const reductions = [
        {before: /\r/g,     after: '\n'}, // carriage returns -> line feeds
        {before: /\n+/g,    after: '\n'}, // line feeds -> line feed
        {before: / +?/g,    after: ' '}, // horizontal whitespaces -> ' '
        {before: /^ +/gm,   after: ''}, // whitespace after line feed -> '' 
    ];

    // Open source file, then initiate rebuild
    fs.readFile(src, 'utf8', (err, data) => rebuild(data, dest, reductions))
}

const rebuild = (data, dest,  [r, ...rest]) => {
    
    // Calculate post-reduction data
    const newData = data.replace(r.before, r.after)
    
    // Rewrite the target file, and rebuild again if reductions remain.
    fs.writeFile(dest, newData, 'utf8', (err) => {
        if (rest.length > 0)
            rebuild(newData, dest, rest);
    })
}

// Initiate minification based on valid command line args
const parseArgs = () => {
    if (process.argv.length == 4) {
        minify(process.argv[2], process.argv[3])
    }
}

const main = () => parseArgs();

main();